PRICE_MULT = 1000


class CustomerAgent:
    # customer is modeled as "energy-reseller", so "demand" is a factor used to convert energy to money
    # max_demand: how much energy the customer is going to buy tops
    def __init__(self, demand_generator):
        self.demand_generator = demand_generator

    #how much energy the customer is going to buy during the next 24 hours
    #price_plan is numpy array of shape (number of price buckets, 24 + 1)
    # 24 + 1 because the first 24 columns are the bucket-prices for each hour of the day and the last the bucket borders
    def get_orders(self, price_plan):
        bucket_borders = price_plan[:,-1]
        price_plan = price_plan[:,:-1]

        orders = []
        demands = self.demand_generator.get_demands()
    #    total_cost = 0
        for hour in range(24):
            prices = price_plan[:,hour]
            order = 0
            profit = 0
            for bucket_num, price in enumerate(prices):
                amount = bucket_borders[bucket_num]

                cost = amount * price
        #        total_cost += cost
                earnings = amount * demands[hour]

                pot_profit = earnings - cost

                if pot_profit > profit:
                    order = amount
                    profit = pot_profit

            orders.append(int(order) * PRICE_MULT)
    #    print("customer pays:", total_cost)
        return orders
