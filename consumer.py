import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt


class Consumer():
    def __init__(self, type, price):
    #    self.name = TODO
        self.price = price # cost per unit of energy (24 hour array of prices)
        self.type = type # erstmal weglassen

    #returns the consumption for the next 24 hours as list wiht 24xn entries
    def get_consumption(self, n):
       func_params = np.array([250.67283324, 500.17196001, 116.26882799, 250.62418395,500.05802045])
       array_total = np.empty((24,0), dtype=float)
       array_multi_price = np.empty((24,0), dtype=float)
       for tag in range(0,10):
           array = np.empty((24,0), dtype=float)
           for i in range(0,24):
                value = consumer_func(i, func_params[0], func_params[1], func_params[2], func_params[3], func_params[4])
                if value < 0:
                    value = value * -1
                array = np.append(array, value, axis=None)
           array_total = np.append(array_total, array)
           array_multi_price = np.append(array_multi_price, array*price)
    return array_multi_price

    def consumer_func(x, a, b, c, d,e):
        rand = random.Random()
        a = a + rand.uniform(-10,10)
        b = b + rand.uniform(-10,10)
        c = c + rand.uniform(-10,10)
        d = d + rand.uniform(-10,10)
        e = e + rand.uniform(-10,10)
        return ((a * np.cos(np.pi * b * (x-e)) * np.multiply(x,x)  + np.cos(np.pi * b * x) * d * np.multiply(x,x)))
