import networkx as nx
import pandas as pd
import numpy as np
from random import shuffle

CAP_MULT = 1
EDGE_LIMIT = 999999999999

class Routing():
    # topology: the routing network
    # customers: list of customer_agents
    def __init__(self, n_producers, n_consumers, n_balancers, worst_sell_price, worst_buy_price):
        self.worst_sell_price = worst_sell_price
        self.worst_buy_price = worst_buy_price

        #reading dataset from csv file
        dataset = pd.read_csv('data/edges.csv', usecols=[0,1,2,3], header=None)
        dataset = dataset.values
        self.dataset = dataset[1:].astype(float)

        #gerenate nodes from dataset
        num_nodes = int(np.asscalar(max(self.dataset[:,:1])))
        assert n_producers + n_consumers + n_balancers < num_nodes

        nodes = list(range(num_nodes))
        shuffle(nodes)

        b1 = n_producers
        b2 = b1 + n_consumers
        b3 = b2 + n_balancers

        self.producer_nodes = nodes[: b1]
        self.consumer_nodes = nodes[b1 : b2]
        self.balance_nodes = nodes[b2 : b3]
        self.inner_nodes = nodes[b3 :]

        self.n_balancers = n_balancers


        self.G = nx.DiGraph()

        for node in nodes:
            self.G.add_node(node)

        #generate edges from dataset
        edges = 0
        for line in self.dataset:
            self.G.add_edge(int(line[0]), int(line[1]), cost=line[2], capacity=line[3] * CAP_MULT)
            edges += 1
            if edges > EDGE_LIMIT:
                break


    def calculate_graph_loss(self, productions, orders):
        total_demand = sum(orders)
        total_production = sum(productions)

        diff = total_production - total_demand

        diff_price = 0
        if diff < 0:
            diff_price = diff * self.worst_buy_price
        if diff > 0:
            diff_price = diff * self.worst_sell_price


        #'''
        for node, prod in zip(self.producer_nodes, productions):
            self.G.node[node]["demand"] = prod * -1

        for node, order in zip(self.consumer_nodes, orders):
            self.G.node[node]["demand"] = order

        for node in self.balance_nodes:
            if self.n_balancers == 1:
                self.G.node[node]["demand"] = diff
            else:
                self.G.node[node]["demand"] = diff / self.n_balancers

        for node in self.inner_nodes:
            self.G.node[node]["demand"] = 0

        flowCost, flowDict = nx.algorithms.flow.capacity_scaling(self.G, weight='cost')

        return flowCost + diff_price


    # calculate how much loss and gain we get based on orders, production and topology
    # productions: list of lists, each of which represents the production of one consumer per hour
    # prod_costs: list of cost per unit of energy (one entry per producer)
    # orders: list of lists, each of which represents how many units of energy a consumer buys per hour
    def calculate_profit(self, productions, prod_costs, orders, price_plan):
        assert len(productions) == len(prod_costs)

        self.bucket_borders = price_plan[:,-1]
        self.price_plan = price_plan[:,:-1]

        #TODO implement non-dummy version
        profit = self.one_node_market(productions, prod_costs, orders, price_plan)
        return profit # NOTE with this line we use only a trivial routing network (a market) for training and
        # demonstration purposes. For real applications you should use a cluster and real data

        productions = np.array(productions)
        orders = np.array(orders)
        routing_losses = [self.calculate_graph_loss(productions[:,hour], orders[:,hour]) for hour in range(24)]

        return profit - sum(routing_losses)

    def one_node_market(self, productions, prod_costs, orders, price_plan):
        profit = 0

        #costs
        for (prod, price) in zip(productions, prod_costs):
            for p in prod:
                profit -= p * price


        #earnings
        for consumer_orders in orders:
            for hour, order in enumerate(consumer_orders):
                profit += self.get_order_price(order, hour)

        return profit


    def get_bucket_num(self, order):
        for bucket_num, border in enumerate(self.bucket_borders):
            if order > border:
                return bucket_num
        return len(self.bucket_borders) - 1


    def get_order_price(self, order, hour):
        bucket = self.get_bucket_num(order)
        return self.price_plan[bucket][hour] * order
