import pandas as pd
from random import random, uniform

class Producer():
    def __init__(self, type, price, id):
        #    self.name = TODO
        self.price = price  # cost per unit of energy
        self.type = type
        self.id = id
        self.databremen = pd.read_csv("data/WinddatenBremen.csv")
        self.datakiel = pd.read_csv("data/WinddatenKiel.csv")
        self.databerlin = pd.read_csv("data/WinddatenBerlin.csv")

    # returns the production for the next 24 hours as list wiht n entries
    def get_production(self):
        if(self.type=="wind"):
            return self.get_wind(self.id)
        if(self.type=="solar"):
            return self.get_solar()
        if(self.type=="dummy"):
            return [1] * 24

    def get_wind(self,id):
        tageswerte = []
        list = []
        for i in range(0, 24):
            list.append(i)
        if (id == 0):
            tageswerte = (self.databremen["Windgeschw."].iloc[0:24])*0.5*1225
            shiftbremen = tageswerte.iloc[0:24]
            self.databremen = self.databremen.drop(list).append(
                shiftbremen).reset_index().drop("index", axis=1)

        elif(id == 1):
            tageswerte = (self.datakiel["Windgeschw."].iloc[0:24])*0.5*1225
            shiftkiel = tageswerte.iloc[0:24]
            self.datakiel = self.datakiel.drop(list).append(
                shiftkiel).reset_index().drop("index", axis=1)

        elif(id == 2):
            tageswerte = (self.databerlin["Windgeschw."].iloc[0:24])*0.5*1225
            shiftberlin = tageswerte.iloc[0:24]
            self.databerlin = self.databerlin.drop(list).append(
                shiftberlin).reset_index().drop("index", axis=1)

        windleistung = tageswerte
        return windleistung


    def get_solar(self):
        solarleistung = []
        zufall = uniform(0.5, 1.5)

        for i in range(0, 24):
            if i < 6:
                solarleistung.append(0)
            elif (i >= 6) & (i <= 18):
                solarleistung.append((-0.08*(i**2)+1.98*i-8.9)*1000*zufall)
            else:
                solarleistung.append(0)
            r = random()
            solarleistung[-1] += solarleistung[-1] *0.2 * r

        return pd.Series(solarleistung)
