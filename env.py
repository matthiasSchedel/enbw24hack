from customer_agent import CustomerAgent
from demand import DemandGenerator
from routing import Routing
from producer import Producer
from random import random
import numpy as np
from math import isnan

class Env():
    def __init__(self):
        self.worst_sell_price = 5 #the default sell price
        self.worst_buy_price = 500 #the default buy price

        self.hours_per_step = 24
        self.n_price_buckets = 5

        self.n_producers = 4
        def prod_cost():
            return 10 + 5 * random()

        #NOTE first "solar" below should be wind but causes sometimes errors
        self.producers = [Producer("solar", prod_cost(), id) for id in range(self.n_producers -1)] + [Producer("solar", prod_cost(), self.n_producers - 1)]
        self.prod_costs = [p.price for p in self.producers]

        self.n_customers  = 3
        self.customers = [CustomerAgent(DemandGenerator()) for _ in range(self.n_customers)]

        n_balancers = 1
        self.routing = Routing(self.n_producers, self.n_customers, n_balancers, self.worst_sell_price, self.worst_buy_price)

    def get_state_dim(self):
        return self.n_producers + self.n_customers

    def get_action_dim(self):
        # agent can pick price progressions for each hour AND the borders of the price buckets (hence the +1 below)
        return self.n_price_buckets * (self.hours_per_step + 1)

    def reset(self):
        self.produced = [0] * self.n_producers
        self.consumed = [0] * self.n_customers

        observation = np.array(self.produced + self.consumed)
        return observation

    def step(self, price_plan):
        price_plan = price_plan.reshape((self.n_price_buckets, self.hours_per_step + 1))
        price_plan = np.maximum(price_plan + 2 * self.worst_sell_price, self.worst_sell_price)
        produced = [p.get_production() for p in self.producers]
    #    for p in produced:
    #        assert len(p) == 24
        consumed = [c.get_orders(price_plan) for c in self.customers]
    #    for c in consumed:
    #        assert len(c) == 24


        profit = self.routing.calculate_profit(produced, self.prod_costs, consumed, price_plan)

        observation = np.array(self.produced + self.consumed)
        reward = profit
        if isnan(reward):
            reward = 0
        done = False
        info = None
        return observation, reward, done, info
