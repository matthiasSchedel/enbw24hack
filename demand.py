from random import random
from math import sin

MAX_DEMAND = 200
NOISE = 20

class DemandGenerator():
    #represents the demand of a customer. For starters this should be simple and stupid (i.e. just some noisy sinuses)
    def __init__(self):
        self.demands = [MAX_DEMAND * random() for _ in range(24)]
        self.t = 0


    def get_demands(self):
        self.t += 1
        return [d + random() * NOISE + self.mod() for d in self.demands]

    def mod(self):
        return 5 * sin(self.t * 10) + 3 * sin(self.t * 1.5) + 3 * sin(self.t + 3)
