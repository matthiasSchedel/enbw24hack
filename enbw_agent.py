
#this represents "us" - reading producer and consumer data, feeding it into an lstm, feeding that into our RL-agent,
# "acting" in form of a price plan and getting rewards based on real customer consumption and routing loss
class ENBWAgent():
    def __init__(self, n_producers, n_customers):
        self.producers = n_producers
        self.customers = n_customers

    # a price plan is a list of list of tuples (amount, price)
    # the outer list represents the time steps the prices are planned for
    # the tuples in the inner list give the price if you buy up to "amount" and at least the "amount" of the previous entry
    def get_price_plan(self, production, consumption):
        #TODO this needs to read in our input data (production and consumption)
        ## TODO: implement
        pass

    def feedback(self, reward):
        ## TODO: implement
        pass
