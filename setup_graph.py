import networkx as nx
import pandas as pd
import matplotlib.pyplot as plt


G = nx.DiGraph()

#reading dataset from csv file
dataset = pd.read_csv('edges.csv', usecols=[0,1,2,3], header=None)
dataset = dataset.values
dataset = dataset[1:].astype(float)

#gerenate nodes from dataset
num_nodes = 10
for i in range(num_nodes):
    G.add_node(i)
G.add_node(10, demand=-5)
G.add_node(11, demand=5)

#generate edges from dataset
for i in range(len(dataset)):
    G.add_edge(int(dataset[i][0]), int(dataset[i][1]), cost=dataset[i][2], capacity=dataset[i][3])
nx.draw(G)

flowCost, flowDict = nx.algorithms.flow.capacity_scaling(G, weight='cost')

print(flowCost)

plt.show()
